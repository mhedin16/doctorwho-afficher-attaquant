 package utilitaires;

import entites.*;
import java.util.Map;
import java.util.TreeMap;
import static jeu.EtatDuJeu.*;

public class Affichages {    

    public  static   Map<String,String> nAb= new TreeMap<String,String>();
    
     static { 
     
         nAb.put("Rory Williams", "rw");
         nAb.put("Jack Hartnet", "jh");
         nAb.put("Mickey Smith", "ms");
         nAb.put("Danny Pink", "dp");    
     }
    
     public static void           affichageEtatDuJeu() {
       
       String messImmun=leDocWho.isDefenseReussie()?"D ":"";
       messImmun+=leDocWho.estImmunise()?" immun":" ";
       System.out.printf("%3d  DOC:%3d %-10s",noPhase,leDocWho.getNbVies(),messImmun);
       System.out.print("Comp: " );
       for( Compagnon cp : compagnons) { 
           System.out.print(nAb.get(cp.getNom())+":");
           if ( ! cp.isEtourdi())System.out.printf("%1d ",cp.getNbVies() );else System.out.print("x ");
       }
   
       System.out.print(" | OODS: " );
       for( Ood o : oods) {
           if( o instanceof  Alpha) System.out.print("a:");else System.out.print(o.getId()+":");
           System.out.printf("%2d ",o.getNbVies() ); 
       }
        System.out.print(" | " );
    }
    
    public static void                       afficherResultatDuCombat(){
        
        if          ( leDocWho.estMort() )  System.out.println("Les Oods ont vaincu le Docteur Who\n");
        else  if ( leDocWho.aFuit()      )  System.out.println("Le Docteur a vaincu les Oods en les fuyant\n");
        else                                          System.out.println("Le Docteur a vaincu les Oods en les tuant\n");
    }   
}