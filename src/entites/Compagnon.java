package entites;
 
import static utilitaires.Hasard.*;

public class Compagnon extends  Personnage {
 
    private String     nom;
    private boolean  etourdi = false;

    public Compagnon(String nom) { this.nom = nom; nbVies = 1; force_max = 6; }
      
    @Override
     public Personnage choisitAdversaire() { return unOodVivantAuHasard(); }
    
    @Override
    public void subitAttaque( int forceAttaque , Personnage attaquant) {
       
        int degats=0;
        
        if( attaquant instanceof  Alpha  && aleat(1, 100)>95) { degats=nbVies; nbVies=0; etourdi=false; return;} 
        
        if (forceAttaque>0){
            if( etourdi) { 
                nbVies=0;degats=nbVies;etourdi=false;
            }
            else{
                int al=aleat(0,10);
                if        ( al<=4)         { nbVies=0; degats=nbVies;}
                else if ( al<=8)         { etourdi=true; degats=-1;}             
            }
        }   
        else if( aleat(1,10)>5) etourdi=false;
        
    }

    @Override
    public void affiche() { System.out.println(nom+"  " +nbVies+ " vies");}

    public boolean  estValide() { return ! etourdi && estVivant(); } 
    public boolean  isEtourdi() { return etourdi;}
    public void       setEtourdi(boolean etourdi) { this.etourdi = etourdi;}
    public String     getNom()                  {return nom;}
   
}
 