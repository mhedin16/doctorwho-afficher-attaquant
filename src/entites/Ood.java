package entites;

import static utilitaires.Hasard.*;
import static jeu.EtatDuJeu.*;

public class Ood extends Personnage{
    
     protected int id;
     
     public Ood()              {  nbVies = aleat(15, 20); force_max=aleat(1,2);}
     public Ood(int id)      { this(); this.id=id; } 
   
      @Override
     public Personnage  choisitAdversaire() {  
    
         if ( ilResteAuMoinsUnCompagnonVivant() && aleat(1, 2)==1 ) return  unCompagnonVivantAuHasard();
         return  leDocWho;    
     }
     
     @Override
     public void            subitAttaque(int forceAttaque,Personnage attaquant)  { 
     
         int degats=forceAttaque<nbVies?forceAttaque:nbVies;
         nbVies -= degats;
         afficherAttaqueSubie(attaquant, forceAttaque, degats);
     }

     @Override
     public void            affiche() {  System.out.println("Ood "+id+" "+ nbVies+" points de vies");}

     public int getId() { return id; }

    @Override
    protected void afficherAttaqueSubie(Personnage attaquant, int forceAttaque, int degats) {
        if(attaquant instanceof Compagnon){
            Compagnon comp = (Compagnon)attaquant;
            System.out.print(comp.getNom()+" attaque Ood "+id+" f="+forceAttaque+" d="+degats);
        }
        else{
            System.out.print("DocWho attaque Ood "+id+" f="+forceAttaque+" d="+degats);
        }
    }
     
     

 }

