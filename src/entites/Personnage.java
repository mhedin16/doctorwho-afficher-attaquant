package entites;

import static utilitaires.Hasard.aleat;

public abstract class Personnage {
    
    protected  int nbVies;
    protected  int force_min=0, force_max; 
   
    public  void                            attaque( Personnage adv, int forceAttaque)  { adv.subitAttaque(forceAttaque, this); }
    public  void                            attaque()                                                     { attaque( choisitAdversaire(), determineForceAttaque() );} 
    
    public abstract Personnage     choisitAdversaire();
    public              int                   determineForceAttaque()  { return aleat(force_min, force_max); }  
   
    public abstract void                subitAttaque(int forceAttaque, Personnage attaquant) ;

   public              boolean            estVivant()                      { return nbVies>0; }  
   public              boolean            estMort()                        { return ! estVivant() ; }
   public abstract void                 affiche();

   public             int                     getNbVies()                     {  return nbVies; }

  public            void                    ajouterUneVie() { nbVies++;}

  protected      void                    afficherAttaqueSubie(Personnage attaquant,int forceAttaque,int degats){
                
        System.out.printf(" attaquant: %-30s, force=%2d, dégats=%2d",attaquant,forceAttaque,degats);
   }
   
}
